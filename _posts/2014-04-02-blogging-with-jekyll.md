---
layout: post
title: "In the Beginning..."
description: ""
cover:
date: 2014-04-02
category:
tags: []
---
## Thanks to Github awesomeness

I now have a digital scratch space hosted using [Github Pages](https://pages.github.com/)! So far Jekyll has been incredibly easy to use, set up, and publish content. It's always nice to be able to push changes via git.

## About me

I'm a current undergraduate at Brown University ('15) studying Applied Math and Computer Science. This blog is really for me to write more and let out ideas banging on my head.

## About the ideas banging around in my head

I like Programming and Math, as you've probably guessed, so I'll be writing mostly on that front. I'm also a fan of Psychology, Life, and Self Organization. I'm generally amazed by anything remotely complex, so those kinds of things start banging around. I'll do my best to make this interesting.

Anyways, that's me.